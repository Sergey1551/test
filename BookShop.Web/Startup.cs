using BookShop.BusinessLogic;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Text;
using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using Microsoft.EntityFrameworkCore;
using BookShop.BusinessLogic.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using BookShop.BusinessLogic.ViewModels;
using BookShop.DataAccess.Entities;
using BookShop.DataAccess;
using BookShop.DataAccess.Interfaces;
using BookShop.DataAccess.EntityRepositories;
using BookShop.DataAccess.DapperRepositories;
using BookShop.BusinessLogic.Services.Interfaces;

namespace BookShop.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddOptions();
            services.Configure<EmailSettings>(Configuration.GetSection("EmailSettings"));

            services.AddAuthorization(options =>
            {
                options.AddPolicy("RequireLoggedIn", policy => policy.RequireRole("Customer", "Admin").RequireAuthenticatedUser());

                options.AddPolicy("RequireAdministratorRole", policy => policy.RequireRole("Admin").RequireAuthenticatedUser());
            });

            services.Configure<ApplicationSettings>(Configuration.GetSection("ApplicationSettings"));

            services.AddCors(options =>
            {
                options.AddPolicy("AllowAllOrigin", builder => builder.AllowAnyOrigin());
            });

            services.AddMvc();

            var appSettingsSections = Configuration.GetSection("ApplicationSettings");
            services.Configure<ApplicationSettings>(appSettingsSections);

            var appSettings = appSettingsSections.Get<ApplicationSettings>();
            var key = Encoding.ASCII.GetBytes(appSettings.Secret);

            services.Configure<ConfigSettings>(Configuration.GetSection("ConfigSettings"));

            services.AddAuthentication(x =>
            {
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultSignInScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(JwtBearerDefaults.AuthenticationScheme, options =>
            {
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidIssuer = appSettings.Site,
                    ValidAudience = appSettings.Audience,
                    IssuerSigningKey = new SymmetricSecurityKey(key)
                };
            });

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddDbContext<ApplicationContext>(options =>
               options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddIdentity<ApplicationUser, IdentityRole>(options => {
                options.Password.RequireDigit = false;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireLowercase = false;
                options.Password.RequireUppercase = false;
                options.User.RequireUniqueEmail = true;
                options.Password.RequiredLength = 4;

                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(5);
                options.Lockout.MaxFailedAccessAttempts = 5;
                options.Lockout.AllowedForNewUsers = true;
            }).AddEntityFrameworkStores<ApplicationContext>().AddDefaultTokenProviders();

            services.AddScoped<IEmailService, EmailService>();
            services.AddScoped<IProductService, ProductService>();
            services.AddScoped<IUserService, UserService>();            
            services.AddScoped<ITokenService, TokenService>();
            services.AddSingleton<DapperBookAuthorRepository>();

            services.AddScoped<ApplicationSettings>();

            int choice = 1;
            if(choice == 0)
            {
                services.AddScoped<ITokenRepository, EntityTokenRepository>();
                services.AddScoped<IProductRepository, EntityProductRepository>();
                services.AddScoped<IAuthorRepository, EntityAuthorRepository>();
                services.AddScoped<IUserRepository, EntityUserRepository>();
                services.AddScoped<IBookAuthorRepository, EntityBookAuthorRepository>();
            }

            if(choice == 1)
            {
                services.AddScoped<ITokenRepository, DapperTokenRepository>();
                services.AddScoped<IProductRepository, DapperProductRepository>();
                services.AddScoped<IAuthorRepository, DapperAuthorRepository>();
                services.AddScoped<IUserRepository, DapperUserRepository>();
                services.AddScoped<IBookAuthorRepository, DapperBookAuthorRepository>();
            }
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseAuthentication();
            app.UseStaticFiles();
            app.UseCors();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}

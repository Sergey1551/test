import { Component, OnInit, ViewChild, TemplateRef, OnDestroy, Output } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { BsModalRef, BsModalService, } from 'ngx-bootstrap/modal';
import { Product } from 'src/app/shared/interface/product';
import { Observable, Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables'
import { ProductService } from 'src/app/shared/services/product.service';
import { AccountService } from 'src/app/shared/services/account.service';
import { EventEmitter } from 'events';
import { IAlert } from 'src/app/shared/interface/IAlert';
import { SharedService } from 'src/app/shared/services/shared.service';
import { author } from 'src/app/shared/interface/author';

@Component({
  selector: 'product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit, OnDestroy {

  cartItemCount: number = 0;
  productAddedToCart: Product[];
  public alerts: Array<IAlert> = [];

  @Output() cartEvent = new EventEmitter();

  insertForm: FormGroup;
  name: FormControl;
  price: FormControl;
  description: FormControl;
  type: FormControl;
  authorIds: FormControl;

  updateForm: FormGroup;
  _name: FormControl;
  _price: FormControl;
  _description: FormControl;
  _type: FormControl;
  _authorIds: FormControl;
  _id: FormControl;

  @ViewChild('template', {static: false}) modal : TemplateRef<any>;

  @ViewChild('editTemplate', {static: false}) editmodal : TemplateRef<any>;

  modalMessage: string; 
  modalRef: BsModalRef;
  selectedProduct: Product;
  selectedAuthors = [];
  products$: Product[];
  authorsList: author[];
  products: Product[] = [];
  userRoleStatus: string;
  types: String[];

  dtOpions: DataTables.Settings = {
    pagingType: 'full_numbers',
    pageLength:5,
    autoWidth: true,
    order: [[0,'desc']]
  };
  dtTrigger: Subject<any> = new Subject();
  
  @ViewChild(DataTableDirective, {static: false}) dtElement : DataTableDirective;

  constructor(private productService: ProductService,
              private modalService: BsModalService,
              private formBuilder: FormBuilder,
              private accountService: AccountService,
              private sharedService: SharedService) { }

  onAddProduct()
  {
    this.insertForm.reset();    
    this.modalRef = this.modalService.show(this.modal);    
  }

  getString(authors: author[])
  { 
    var result = authors.map(a => a.name.toString());
    return result;
  }

  isProductInStock(product: Product)
  {
    if(product.outOfStock == true)
    {
      return "No";
    }

    if(product.outOfStock == false)
    {
      return "Yes";
    }
  }

  onUpdate()
  { 
    let editProduct = this.updateForm.value;

    this.productService.updateProduct(editProduct.id, editProduct).subscribe(
      result => 
      {
        this.productService.clearCache();
        this.productService.getProducts().subscribe(res => {
          this.products = res;
        });

        console.log("Product updated");
      },

      error => console.log("Could not update product"))
  }

  onUpdateModal(productEdit : Product) : void
  {
    this._id.setValue(productEdit.productId);
    this._name.setValue(productEdit.name);
    this._price.setValue(productEdit.price);
    this._description.setValue(productEdit.description);
    var autorIds: number[] = [];
    productEdit.authors.forEach(res=> {
      autorIds.push(res.id)
    })
    
    this._authorIds.setValue(autorIds);
    this._type.setValue(productEdit.type);
    
    this.updateForm.setValue({
      'id': this._id.value,
      'name': this._name.value,
      'price': this._price.value,
      'description': this._description.value,
      'authorIds': this._authorIds.value,
      'type': this._type.value, 
      'outOfStock': true
    });

    this.modalRef = this.modalService.show(this.editmodal);
  }

  onDelete(product : Product) : void 
  {
    this.productService.deleteProduct(product.productId).subscribe(result => 
      {
        this.productService.clearCache();
        this.productService.getProducts().subscribe(newList =>
          {
            this.products = newList;
          })
      })
  }

  onAddToCart(product: Product)
  {
    console.log(product);
    
    this.productAddedToCart=this.productService.getProductFromCart();
    if(this.productAddedToCart == null)
    {
      this.productAddedToCart = [];
      this.productAddedToCart.push(product);
      this.productService.addProductToCart(this.productAddedToCart);
      this.alerts.push({
        id: 1,
        type: 'success',
        message: 'Product added to cart.'
      });

      setTimeout(() => {   
        this.closeAlert(this.alerts);
      }, 3000);
    }
    
    if(this.productAddedToCart != null)
    {
      let tempProduct = this.productAddedToCart.find(p => p.productId == product.productId);
      if(tempProduct == null)
      {
        this.productAddedToCart.push(product);
        this.productService.addProductToCart(this.productAddedToCart);
        this.alerts.push({
          id: 1,
          type: 'success',
          message: 'Product added to cart.'
        });
        setTimeout(() => {   
          this.closeAlert(this.alerts);
        }, 3000);
      }
    
      this.alerts.push({
        id: 2,
        type: 'warning',
        message: 'Product already exist in cart.'
      });
      setTimeout(() => {   
        this.closeAlert(this.alerts);
      }, 3000);            
    }

    this.cartItemCount = this.productAddedToCart.length;
    this.sharedService.updateCartCount(this.cartItemCount);
  }
  
  public closeAlert(alert: any) 
  {
    const index: number = this.alerts.indexOf(alert);
    this.alerts.splice(index, 1);
  }

  onSubmit()
  {debugger
    let newProduct = this.insertForm.value;

    this.productService.insertProduct(newProduct).subscribe(
      result => 
      {
        this.productService.clearCache();

        this.productService.getProducts().subscribe(newList => {
          this.products = newList;
          this.modalRef.hide();
          this.insertForm.reset();
          this.dtTrigger.next();
        });

        console.log("New product added");
      },

    error => console.log("Could not add product"))
  }

  ngOnDestroy()
  {
    this.dtTrigger.unsubscribe();
  }

  ngOnClear()
  {
    this.insertForm.reset();
  }

  ngOnInit() 
  {
    this.types = ['Book','Journal'];
    this.productService.getAuthors().subscribe(res => {
      console.log(res)
      this.authorsList = res;
    });
    localStorage.removeItem('product');

    this.productService.getProducts().subscribe(result => {
      this.products = result;

      this.dtTrigger.next();
    });

    this.accountService.currentUserRole.subscribe(result => {this.userRoleStatus = result});

    this.modalMessage = "All fields are mandatory";

    this.name = new FormControl('',[Validators.required, Validators.maxLength(50)]);
    this.price = new FormControl('',[Validators.required, Validators.min(0), Validators.max(1000000000)]);
    this.description = new FormControl('',[Validators.required, Validators.maxLength(150)]);
    this.type = new FormControl('',[Validators.required, Validators.maxLength(50)]);
    this.authorIds = new FormControl('',[Validators.required]);

    this.insertForm = this.formBuilder.group({
      'name': this.name,
      'price': this.price,
      'description': this.description,
      'type': this.type,
      'outOfStock': true,
      'authorIds': this.authorIds,
    });

    this._name = new FormControl('',[Validators.required, Validators.maxLength(50)]);
    this._price = new FormControl('',[Validators.required, Validators.min(0), Validators.max(1000000000)]);
    this._description = new FormControl('',[Validators.required, Validators.maxLength(150)]);
    this._type = new FormControl('',[Validators.required, Validators.maxLength(50)]);
    this._authorIds = new FormControl('',[Validators.required]);
    this._id = new FormControl();

    this.updateForm = this.formBuilder.group({
      'id' : this._id,
      'name': this._name,
      'price': this._price,
      'description': this._description,
      'type': this._type,
      'outOfStock': true,
      'authorIds': this._authorIds,
    });
  }
}

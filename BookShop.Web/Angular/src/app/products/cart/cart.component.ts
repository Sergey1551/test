import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { IAlert } from 'src/app/shared/interface/IAlert';
import { Product } from 'src/app/shared/interface/product';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { ProductService } from 'src/app/shared/services/product.service';
import { User } from 'src/app/shared/interface/user';
import { OrderItem } from 'src/app/shared/interface/order-item';
import { OrderDetail } from 'src/app/shared/interface/order-details';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { CreditCardValidator } from 'angular-cc-library';
import { PaymentService } from 'src/app/shared/services/payment.service';

@Component({
  selector: 'cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  cardForm: FormGroup;
  email: FormControl;
  cardNumber: FormControl;
  expirationDate: FormControl;
  cvc: FormControl;

  dafualtQuantity: number = 1;
  productAddedTocart: Product[];
  allTotal: number;
  currentUser: User[];
  orderDetail: OrderDetail;
  orderItem: OrderItem[];

  result: any;

  modalRef: BsModalRef;
  
  public globalResponse: any;
  public alerts: Array<IAlert> = [];

  deliveryForm: FormGroup;

  @ViewChild('template', {static: false}) modal : TemplateRef<any>;

  constructor(private productService: ProductService,
              private formBuilder: FormBuilder,
              private modalService: BsModalService,
              private paymentService: PaymentService) {}

  ngOnInit() {
    this.productAddedTocart=this.productService.getProductFromCart();
    for (let i in this.productAddedTocart) {
      this.productAddedTocart[i].quantity = 1;
    }

    this.cardForm = this.formBuilder.group({
      creditCard: ['', [<any>CreditCardValidator.validateCCNumber]],
      expirationDate: ['', [<any>CreditCardValidator.validateExpDate]],
      cvc: ['', [<any>Validators.required, <any>Validators.minLength(3), <any>Validators.maxLength(4)]] 
    });

    this.productService.removeAllProductFromCart();
    this.productService.addProductToCart(this.productAddedTocart);
    this.calculteAllTotal(this.productAddedTocart);

    this.deliveryForm = this.formBuilder.group({
    UserName:  ['', [Validators.required]],
    DeliveryAddress:['',[Validators.required]],
    Phone:['',[Validators.required]],
    Email:['',[Validators.required]],
    Message:['',[]],
    Amount:['',[Validators.required]],
    });
  }

  onCard()
  { 
    this.modalRef = this.modalService.show(this.modal);
  }

  openCheckout() {
    var handler = (<any>window).StripeCheckout.configure({
      key: 'pk_test_oi0sKPJYLGjdvOXOM8tE8cMa',
      locale: 'auto',
      token: function (token: any) {
      }
    });

    handler.open({
      name: 'Demo Site',
      description: '2 widgets',
      amount: 2000
    });
  }

  onSubmit()
  {
    this.result = this.paymentService.paymentOk()
    console.log(this.result)
  }

  onAddQuantity(product: Product)
  {
    this.productAddedTocart = this.productService.getProductFromCart();
    this.productAddedTocart.find(p=>p.productId == product.productId).quantity = product.quantity+1;

    this.productService.removeAllProductFromCart();
    this.productService.addProductToCart(this.productAddedTocart);
    this.calculteAllTotal(this.productAddedTocart);
    this.deliveryForm.controls['Amount'].setValue(this.allTotal);   
  }

  onRemoveQuantity(product: Product)
  {
    this.productAddedTocart = this.productService.getProductFromCart();
    this.productAddedTocart.find(p => p.productId == product.productId).quantity = product.quantity-1;
    
    this.productService.removeAllProductFromCart();
    this.productService.addProductToCart(this.productAddedTocart);
    this.calculteAllTotal(this.productAddedTocart);
    this.deliveryForm.controls['Amount'].setValue(this.allTotal);
  }

  calculteAllTotal(allItems: Product[])
  {
    let total = 0;
    for (let i in allItems) {
      total = total + (allItems[i].quantity * allItems[i].price);
   }

   this.allTotal = total;
  }

  ConfirmOrder()
  {
    const date: Date = new Date();
    var id = this.currentUser['Id'];
    var name = this.currentUser["UserName"];
    var day = date.getDate();
    var monthIndex = date.getMonth();
    var year = date.getFullYear();
    var minutes = date.getMinutes();
    var hours = date.getHours();
    var seconds = date.getSeconds();
    var dateTimeStamp = day.toString() + monthIndex.toString() + year.toString() + minutes.toString() + hours.toString()+seconds.toString();
    let orderDetail:any = {};
    
    orderDetail.CustomerId=this.currentUser['Id'];
    orderDetail.CustomerName=this.currentUser["UserName"];
    orderDetail.DeliveryAddress = this.deliveryForm.controls['DeliveryAddress'].value;
    orderDetail.Phone = this.deliveryForm.controls['Phone'].value;

    orderDetail.PaymentRefrenceId = id + "-" + name + dateTimeStamp;
    orderDetail.OrderPayMethod = "Cash On Delivery";
    
    this.orderItem = [];
    for (let i in this.productAddedTocart) {
      this.orderItem.push({
        ID:0,
        ProductID: this.productAddedTocart[i].productId,
        ProductName: this.productAddedTocart[i].name,
        OrderedQuantity: this.productAddedTocart[i].quantity,
        PerUnitPrice: this.productAddedTocart[i].price,
        OrderID: 0,
      });
    }
  }   
}
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { AccountService } from '../shared/services/account.service';
import { ProductService } from "../shared/services/product.service";

@Component({
  selector: 'nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.css']
})
export class NavMenuComponent implements OnInit {

  constructor(private accountService: AccountService, private productservice: ProductService) { }

  userRoleStatus$: Observable<string>;
  loginStatus$: Observable<boolean>;
  userName$: Observable<string>

  ngOnInit() {

    this.userRoleStatus$ = this.accountService.currentUserRole;

    this.loginStatus$ = this.accountService.isLoggedIn;

    this.userName$ = this.accountService.currentUserName;
  }
 
  onLogout(){
    this.productservice.clearCache()
    this.accountService.logout();
  }
}

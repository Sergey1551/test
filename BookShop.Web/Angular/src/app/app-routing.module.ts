import { NgModule, Host } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './identity/login/login.component';
import { RegisterComponent } from './identity/register/register.component';
import { AccessDeniedComponent } from './errors/access-denied/access-denied.component';
import { UserComponent } from './user/user.component';
import { AuthGuardService } from './shared/guards/auth-guard.service';
import { CartComponent } from './products/cart/cart.component';

const routes: Routes = [];

@NgModule({
  imports: [RouterModule.forRoot([
    { path: "home", component: HomeComponent},
    { path: 'login', component: LoginComponent},
    { path: 'products', loadChildren: './products/products.module#ProductsModule'},
    { path: 'user', component: UserComponent, canActivate: [AuthGuardService] },
    {path: "cart", component: CartComponent} ,
    { path:'', component: HomeComponent, pathMatch: 'full'},
    { path: 'register', component: RegisterComponent },
    { path: 'access-denied', component: AccessDeniedComponent },
    { path:'**', redirectTo: '/home'},
  ])],
  exports: [RouterModule]
})
export class AppRoutingModule { }

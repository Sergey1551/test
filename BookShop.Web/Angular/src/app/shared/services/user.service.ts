import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { shareReplay } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { User } from '../interface/user';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private user$: Observable<User[]>

  constructor(private http: HttpClient) { }

  private userUrl = environment.userUrl;

  getUsers() : Observable<User[]>
  {
    if(!this.user$)
    {
      this.user$ = this.http.get<User[]>(this.userUrl + "getusers").pipe(shareReplay());
    }
    
    return this.user$;
  }

  newUser(userName: string, email: string, emailConfirmed: boolean, password: string) : Observable<User[]>
  {
    return this.http.post<any>(this.userUrl + "add", {userName, email, emailConfirmed, password});
  }

  deleteUser(userName: string) : Observable<any>
  {
    return this.http.delete(this.userUrl + "deleteuser/" + userName);
  }  

  clearCache()
  {
    this.user$ = null;
  }
}

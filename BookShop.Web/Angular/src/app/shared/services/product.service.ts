import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Product } from '../interface/product';
import { shareReplay} from 'rxjs/operators';
import { author } from '../interface/author';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http: HttpClient) { }

  private productUrl = environment.productUrl;

  private product$: Observable<Product[]>;

  getProducts() : Observable<Product[]>
  {
    if(!this.product$)
    {
      this.product$ = this.http.get<Product[]>(this.productUrl + "getproducts")
    }
    
    return this.product$;
  }

  getAuthors() : Observable<author[]>
  {    
    return this.http.get<author[]>(this.productUrl + "getauthors")
  }

  insertProduct(newProduct: Product) : Observable<Product>
  {debugger
    return this.http.post<Product>(this.productUrl + "addproduct", newProduct);
  }

  updateProduct(id: number, editProduct: Product) : Observable<Product>
  {
    return this.http.put<Product>(this.productUrl + "updateproduct/" + id, editProduct);
  }

  deleteProduct(id: number) : Observable<any>
  {
    return this.http.delete(this.productUrl + "deleteproduct/" + id);
  }

  clearCache()
  {
    this.product$ = null;
  }

  addProductToCart(productAddedToCart: Product[]) 
  {
    localStorage.setItem("product", JSON.stringify(productAddedToCart));
  }

  getProductFromCart() 
  {
    return JSON.parse(localStorage.getItem('product'));
  }

  removeAllProductFromCart() 
  {
    return localStorage.removeItem("product");
  }
}


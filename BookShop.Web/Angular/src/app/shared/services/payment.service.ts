import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PaymentService {

  constructor(private http: HttpClient) { }

  private baseUrl = environment.baseUrl;;

  paymentOk()
  {
    return this.http.get(this.baseUrl + "/api/payment/paymentOk");
  }
}

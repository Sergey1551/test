import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AccountService } from '../services/account.service';
import { take, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService  implements CanActivate {

  constructor(private accountService: AccountService, private router: Router) { }

  canActivate(route :ActivatedRouteSnapshot, state : RouterStateSnapshot) : Observable<boolean>
  {
    return this.accountService.isLoggedIn.pipe(take(1), map((loginStatus: boolean) =>
    {
      const destination: string = state.url;
      const productId = route.params.id;
      const _userRole = localStorage.getItem("userRole");

      if(!loginStatus)
      {
        this.router.navigate(['/login'], {queryParams: {returnUrl: state.url}});

        return false;
      }
      
      if((destination == '/products' || destination == '/products' + productId) && (_userRole === "Admin" || _userRole === "Customer"))
      {
        return true;
      }

      if(destination == '/products/update' && _userRole === "Customer")
      {
        this.router.navigate(['/access-denied']);

        return false;
      }

      if(destination == '/product/update' && _userRole === "Admin")
      {
        return true;
      }

      if((destination == '/user') && (_userRole ==="Admin"))
      {
        return true;
      }

      return false;
    }))
  }
}

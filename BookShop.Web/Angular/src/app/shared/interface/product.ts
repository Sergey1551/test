import { author } from './author';

export interface Product {
    productId: number;
    name: string;
    description: string;
    outOfStock: boolean;
    price: number;
    type: string;
    authors: author[];
    quantity: number;    
}

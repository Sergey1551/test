export interface User {
    userName: string;
    email: string,
    emailConfirmed: boolean,
    password: string
}

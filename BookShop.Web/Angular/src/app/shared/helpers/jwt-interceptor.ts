import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { AccountService } from '../services/account.service';
import { Observable, BehaviorSubject, throwError } from 'rxjs';
import { tap, catchError, switchMap, finalize, filter, take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
    export class JwtInterceptor implements HttpInterceptor {

    private isTokenRefreshing: boolean = false;

    tokenSubject: BehaviorSubject<string> = new BehaviorSubject<string>(null);

    constructor (private accountService : AccountService) {}

    intercept(request : HttpRequest<any>, next : HttpHandler): Observable<HttpEvent<any>> 
    {
        return next.handle(this.attachTokenToRequest(request)).pipe(
            tap((event : HttpEvent<any>) => {
                if(event instanceof HttpResponse) 
                {
                    console.log("Success");
                }
            }),
            catchError((err) : Observable<any> => {  
                if((err instanceof HttpErrorResponse) && ((<HttpErrorResponse>err).status == 401)) 
                {
                    console.log("Token expired. Attempting refresh ...");
                    return this.handleHttpResponseError(request, next);
                }

                if((err instanceof HttpErrorResponse) && ((<HttpErrorResponse>err).status == 400))
                {
                   console.error(err);
                }               

                return throwError(this.handleError);                
            })
        );
    }

    private handleError(errorResponse : HttpErrorResponse) 
    {
        let errorMsg : string;

        if(errorResponse.error instanceof Error) 
        {            
            errorMsg = "An error occured : " + errorResponse.error.message;
        }  

        errorMsg = `Backend returned code ${errorResponse.status}, body was: ${errorResponse.error}`;
        return throwError(errorMsg);
    }

    private handleHttpResponseError(request : HttpRequest<any>, next : HttpHandler) 
    {
        if(!this.isTokenRefreshing) 
        {
            this.isTokenRefreshing = true;

            this.tokenSubject.next(null);

            return this.accountService.getNewRefreshToken().pipe(
                switchMap((tokenresponse: any) => {
                if(tokenresponse) 
                {
                    this.tokenSubject.next(tokenresponse.authToken.token); 
                    localStorage.setItem('loginStatus', '1');
                    localStorage.setItem('jwt', tokenresponse.authToken.token);
                    localStorage.setItem('username', tokenresponse.authToken.userName);
                    localStorage.setItem('expiration', tokenresponse.authToken.expiration);
                    localStorage.setItem('userRole', tokenresponse.authToken.roles);
                    localStorage.setItem('refreshToken', tokenresponse.authToken.refreshToken);
                    console.log("Token refreshed...");
                    return next.handle(this.attachTokenToRequest(request));

                }
                    return <any>this.accountService.logout();
                }),
                catchError(err => {
                    this.accountService.logout();
                    return this.handleError(err);
                }),
                finalize(() => {
                  this.isTokenRefreshing = false;
                })
            );
        }

        if(this.isTokenRefreshing) 
        {
            this.isTokenRefreshing = false;
            return this.tokenSubject.pipe(filter(token => token != null),
                take(1),
                switchMap(token => {
                return next.handle(this.attachTokenToRequest(request));
            }));
        }
    }

    private attachTokenToRequest(request: HttpRequest<any>) 
    {
        var token = localStorage.getItem('jwt');

        return request.clone({setHeaders: {Authorization: `Bearer ${token}`}});
    }
}
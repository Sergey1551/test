import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators, AbstractControl, ValidatorFn } from '@angular/forms';
import { AccountService } from '../../shared/services/account.service';
import { Router } from '@angular/router';

@Component({
  selector: 'register',
  templateUrl: './register.component.html',
})
export class RegisterComponent implements OnInit {

    constructor(
          private formBuilder: FormBuilder,
          private accountService: AccountService,
          private router: Router,
          ) { }

    insertForm: FormGroup;
    userName: FormControl;
    password: FormControl;
    confirmPassword: FormControl;
    email: FormControl;
    errorList: string[];
    modalMessage: string;  

    test: any[] = [];

    onSubmit() 
    {
        let userDetails = this.insertForm.value;

        this.accountService.register(userDetails.userName, userDetails.password, userDetails.email).subscribe(result => 
        {          
            this.router.navigate(['/login']);
        }, error => 
        {          
            console.log(error)
            this.modalMessage = "Your Registration Was Unsuccessful";
        });
    }

    MustMatch(passwordControl: AbstractControl) : ValidatorFn
    {
        return (confirmPasswordControl: AbstractControl) : {[key: string]: boolean } | null   => 
        {
           
            if(!passwordControl && !confirmPasswordControl) 
            {
                return null;
            }
            
            if (confirmPasswordControl.hasError && !passwordControl.hasError) 
            {
                return null;
            } 

            if(passwordControl.value !== confirmPasswordControl.value) 
            {
                return { 'mustMatch': true };
            }
            else {
                return null;
            }
        } 
    }

    ngOnInit() {

         this.userName = new FormControl('', [Validators.required]);
         this.password = new FormControl('', [Validators.required]);
         this.confirmPassword = new FormControl('',[Validators.required, this.MustMatch(this.password)]);
         this.email = new FormControl('', [Validators.required, Validators.email]);
         this.errorList = [];

        this.insertForm = this.formBuilder.group(
        {
                'userName': this.userName,
                'password': this.password,
                'confirmPassword': this.confirmPassword,
                'email': this.email,
        });
    }     
}
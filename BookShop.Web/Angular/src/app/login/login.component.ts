import { Component, OnInit } from '@angular/core';
import { AccountService } from '../shared/services/account.service';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from "@angular/router";

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

    insertForm: FormGroup;
    userName:  FormControl;
    password:  FormControl;
    returnUrl: string;
    errorMessage: string;
    invalidLogin: boolean;  

    constructor(private accountService: AccountService,
                private router: Router,
                private route: ActivatedRoute,
                private formBuilder: FormBuilder)
                { }

    onSubmit() 
    {
        let userLogin = this.insertForm.value;

        this.accountService.login(userLogin.userName, userLogin.password).subscribe(result => {

            let token = (<any>result).token;
            console.log(result.authToken.token);
            console.log(result.authToken.roles);
            console.log("User Logged In Successfully");
            this.invalidLogin = false;
            console.log(this.returnUrl);
            this.router.navigateByUrl(this.returnUrl);        
        },
        error => 
        {
            this.invalidLogin = true;

            this.errorMessage = error.error.loginError;

            console.log(this.errorMessage);
        })
    }

    ngOnInit() {

        this.userName = new FormControl('', [Validators.required]);
        this.password = new FormControl('', [Validators.required]);

        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';

        this.insertForm = this.formBuilder.group({
            "userName": this.userName,
            "password": this.password        
        });
    }
}

import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators} from '@angular/forms';
import { Subject, Observable } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { UserService } from '../shared/services/user.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { User } from '../shared/interface/user';

@Component({
  selector: 'user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  insertForm: FormGroup;
  userName: FormControl;
  email: FormControl;
  emailConfirmed: FormControl;  
  password: FormControl;
  confirmPassword: FormControl;

  @ViewChild('userTemplate', {static: false}) modal : TemplateRef<any>;

  modalRef: BsModalRef;
  users$: Observable<User[]>;
  users: User[] = [];

  dtOpions: DataTables.Settings = {
    pagingType: 'full_numbers',
    pageLength: 5,
    autoWidth: true,
    order: [[0,'desc']]
  };

  dtTrigger: Subject<any> = new Subject();
  
  @ViewChild(DataTableDirective, {static: false}) dtElement : DataTableDirective;

  constructor(private userService : UserService,
              private modalService : BsModalService,
              private formBuilder : FormBuilder) { }

  onAddUser()
  {
    this.insertForm.reset();    
    this.modalRef = this.modalService.show(this.modal);
  }

  ngOnClear()
  {
    this.insertForm.reset();
  }

  onDelete(user : User) : void 
  {
    this.userService.deleteUser(user.userName).subscribe(result => 
      {
        this.userService.clearCache();
        this.users$ = this.userService.getUsers();
        this.users$.subscribe(newList =>
          {
            this.users = newList;
          })
      })
  }

  ngOnInit() {

    this.users$ = this.userService.getUsers()

    this.users$.subscribe(result => {
      this.users = result;

      this.dtTrigger.next();
    });

    this.userName = new FormControl('',[Validators.required, Validators.maxLength(50)]);
    this.email = new FormControl('',[Validators.required, Validators.maxLength(50)]);
    this.emailConfirmed = new FormControl('',[Validators.required, Validators.maxLength(50)]);
    this.password = new FormControl('',[Validators.required,Validators.maxLength(50)])

    this.insertForm = this.formBuilder.group({
      'userName': this.userName,
      'email': this.email,
      'emailConfirmed': false,
      'password': this.password
    });
  }

  onSubmit()
  {
    let newUser = this.insertForm.value;

    this.userService.newUser(newUser.userName, newUser.email, newUser.emailConfirmed, newUser.password).subscribe(
      result => 
      {
        this.userService.clearCache();
        this.users$ = this.userService.getUsers();

        this.users$.subscribe(newList => {
          this.users = newList;
          this.modalRef.hide();
          this.insertForm.reset();
          this.dtTrigger.next();
        });

        console.log("New user added");
      },

      error => console.log("Could not add user"))
  }  
}

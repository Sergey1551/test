import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'access-denied',
  templateUrl: './access-denied.component.html',
  styleUrls: ['./access-denied.component.css']
})
export class AccessDeniedComponent implements OnInit {

  error: string;
  accessDenied: string;

  constructor() { }

  ngOnInit() {
    this.error = "403";
    this.accessDenied = "Access Denied";
  }
}

﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Identity;
using System;
using System.Threading.Tasks;
using BookShop.BusinessLogic.ViewModels;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using System.Text;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.Extensions.Options;
using System.Linq;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using BookShop.BusinessLogic;
using BookShop.DataAccess.Entities;
using BookShop.BusinessLogic.Services.Interfaces;

namespace BookShop.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class IdentityController : ControllerBase
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ApplicationSettings _appSettings;
        private readonly IEmailService _emailService;

        public IdentityController(UserManager<ApplicationUser> userManager,
                                  IOptions<ApplicationSettings> appSettings,
                                  IEmailService emailService)
        {
            _userManager = userManager;
            _appSettings = appSettings.Value;
            _emailService = emailService;
        }

        [EnableCors("AllowAllOrigin")]
        [HttpPost("[action]")]
        public async Task<IActionResult> Register([FromBody] RegisterIdentityView model)
        {
            List<string> errorList = new List<string>();

            var user = new ApplicationUser
            {
                Email = model.Email,
                UserName = model.UserName,
                SecurityStamp = Guid.NewGuid().ToString()
            };

            var result = await _userManager.CreateAsync(user, model.Password);

            if (result.Succeeded)
            {
                await _userManager.AddToRoleAsync(user, "Customer");

                var code = _userManager.GenerateEmailConfirmationTokenAsync(user).Result;

                var callbackUrl = Url.Action("EmailConfirm", "Identity", new { UserId = user.Id, Code = code }, protocol: HttpContext.Request.Scheme);

                await _emailService.SendEmailAsync(user.Email, " - Confirm Your Email", "Please confirm your e-mail by clicking this link: <a href=\"" + callbackUrl + "\">click here</a>");

                return Ok(new { username = user.UserName, email = user.Email, status = 1, message = "Registration Successful" });

            }

            if(!result.Succeeded)
            {
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError(string.Empty, error.Description);
                    errorList.Add(error.Description);
                }
            }

            return BadRequest(new JsonResult(errorList));
        }

        [EnableCors("AllowAllOrigin")]
        [HttpPost]
        [Route("login")]
        public async Task<IActionResult> Login(LoginIdentityView model)
        {
            var user = await _userManager.FindByNameAsync(model.UserName);

            var roles = await _userManager.GetRolesAsync(user);

            var key = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(_appSettings.Secret));

            double tokenExpireTime = Convert.ToDouble(_appSettings.ExpireTime);

            if (!await _userManager.IsEmailConfirmedAsync(user))
            {
                ModelState.AddModelError(string.Empty, "User Has not Confirmed Email.");

                return Unauthorized();
            }

            if (user != null && await _userManager.CheckPasswordAsync(user, model.Password))
            {
                
                var tokenHandler = new JwtSecurityTokenHandler();
                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new Claim[]
                    {
                        new Claim(JwtRegisteredClaimNames.Sub, model.UserName),
                        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                        new Claim(ClaimTypes.NameIdentifier, user.Id),
                        new Claim(ClaimTypes.Role, roles.FirstOrDefault()),
                        new Claim("LoggedOn", DateTime.UtcNow.ToString()),
                    }),
                    Issuer = _appSettings.Site,
                    Audience = _appSettings.Audience,
                    Expires = DateTime.UtcNow.AddMinutes(tokenExpireTime),
                    SigningCredentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256Signature)
                };
                var token = tokenHandler.CreateToken(tokenDescriptor);

                return Ok(new { token = tokenHandler.WriteToken(token), expiration = token.ValidTo, username = user.UserName, userRole = roles.FirstOrDefault() });
            }

            return BadRequest(new { message = "Username or password is incorrect" });
        }
        
        [HttpGet("[action]")]
        [AllowAnonymous]
        public async Task<IActionResult> EmailConfirm(string userId, string code)
        {            
            if (string.IsNullOrWhiteSpace(userId) || string.IsNullOrWhiteSpace(code))
            {
                ModelState.AddModelError(string.Empty, "User Id and Code are required");

                return BadRequest(ModelState);
            }

            var user = await _userManager.FindByIdAsync(userId);

            if(user == null)
            {
                return new JsonResult("ERROR");
            }

            var result = await _userManager.ConfirmEmailAsync(user, code);

            if (!result.Succeeded)
            {
                List<string> errors = new List<string>();
                foreach (var error in result.Errors)
                {
                    errors.Add(error.ToString());
                }

                return new JsonResult(errors);
            }

            return BadRequest();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BookShop.BusinessLogic.Services.Interfaces;
using BookShop.BusinessLogic.ViewModels;
using BookShop.DataAccess.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace BookShop.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IUserService _userService;

        public UserController(UserManager<ApplicationUser> userManager, IUserService userService)
        {
            _userManager = userManager;
            _userService = userService;
        }

        [HttpGet("[action]")]
        [Authorize(AuthenticationSchemes = "Bearer", Policy = "RequireAdministratorRole")]
        public async Task<IActionResult> GetUsers()
        {
            return Ok(await _userService.GetListOfUsers());
        }

        [HttpPost("[action]")]
        [Authorize(AuthenticationSchemes = "Bearer", Policy = "RequireAdministratorRole")]
        public async Task<IActionResult> Add(AddUserView model)
        {
            List<string> errorList = new List<string>();

            var user = new ApplicationUser
            {
                UserName = model.UserName,
                Email = model.Email,
                SecurityStamp = Guid.NewGuid().ToString(),
                EmailConfirmed = model.EmailConfirmed
            }; 

            var result = await _userManager.CreateAsync(user, model.Password);

            if (result.Succeeded)
            {
                await _userManager.AddToRoleAsync(user, "Customer");       

                return Ok(new { username = user.UserName, email = user.Email, status = 1, message = "New user Added" });

            }

            if (!result.Succeeded)
            {
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError(string.Empty, error.Description);
                    errorList.Add(error.Description);
                }
            }

            return BadRequest(new JsonResult(errorList));
        }

        [HttpDelete("[action]/{userName}")]
        [Authorize(AuthenticationSchemes = "Bearer", Policy = "RequireAdministratorRole")]
        public async Task<IActionResult> DeleteUser([FromRoute] string userName)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var foundUser = await _userService.FindUser(userName);

            if (foundUser == null)
            {
                return NotFound();
            }

            await _userService.DeleteUserAsync(foundUser);

            return Ok(new JsonResult("The User has been deleted"));
        }
    }
}
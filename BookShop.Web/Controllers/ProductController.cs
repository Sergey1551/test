﻿using System.Threading.Tasks;
using BookShop.BusinessLogic.Services.Interfaces;
using BookShop.BusinessLogic.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BookShop.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IProductService _productService;

        public ProductController(IProductService productService)
        {
            _productService = productService;
        }

        [HttpGet("[action]")]
        [Authorize(AuthenticationSchemes = "Bearer", Policy = "RequireLoggedIn")]
        public async Task<IActionResult> GetProducts()
        {
           return Ok(await _productService.GetListOfProducts());
        }

        [HttpGet("[action]")]
        [Authorize(AuthenticationSchemes = "Bearer", Policy = "RequireLoggedIn")]
        public async Task<IActionResult> GetAuthors()
        {
            return Ok( await _productService.GetListOfAuthors());
        }

        [HttpPost("[action]")]
        [Authorize(AuthenticationSchemes = "Bearer", Policy = "RequireAdministratorRole")]
        public async Task<IActionResult> AddProduct(AddProductView model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var result = await _productService.AddProductAsync(model);

            return result;
        }

        [HttpPost("[action]")]
        [Authorize(AuthenticationSchemes = "Bearer", Policy = "RequireAdministratorRole")]
        public async Task<IActionResult> AddAuthor(AddAuthorProductView model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var result = await _productService.AddAuthorAsync(model);

            return result;
        }

        [HttpPut("[action]/{id}")]
        [Authorize(AuthenticationSchemes = "Bearer", Policy = "RequireAdministratorRole")]
        public async Task<IActionResult> UpdateProduct([FromRoute] int id, [FromBody] UpdateProductView model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            await _productService.UpdateProductAsync(id ,model);

            return Ok(new JsonResult("The Product with id " + id + " is updated"));
        }

        [HttpDelete("[action]/{id}")]
        [Authorize(AuthenticationSchemes = "Bearer", Policy = "RequireAdministratorRole")]
        public async Task<IActionResult> DeleteProduct([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var foundProduct = await _productService.FindProduct(id);

            if (foundProduct == null)
            {
                return NotFound();
            }

            await _productService.DeleteProductAsync(foundProduct);

            return Ok(new JsonResult("The Product with id " + id + " is deleted"));
        }
    }
}
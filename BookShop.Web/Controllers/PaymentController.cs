﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BookShop.Web.Controllers
{
   
    [Route("api/[controller]")]
    [ApiController]
    public class PaymentController : ControllerBase
    {
        [HttpGet("[action]")]
        [Authorize(AuthenticationSchemes = "Bearer", Policy = "RequireLoggedIn")]
        public IActionResult PaymentOk()
        {
            return Ok(new JsonResult("We got your money :)"));
        }
    }
}
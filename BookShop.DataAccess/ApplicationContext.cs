﻿using BookShop.DataAccess.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace BookShop.DataAccess
{
    public class ApplicationContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationContext(DbContextOptions<ApplicationContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<IdentityRole>().HasData(
                    new { Id = "1", Name = "Admin", NormalizedName = "ADMIN" },
                    new { Id = "2", Name = "Customer", NormalizedName = "CUSTOMER" }
                );


            builder.Entity<Author>()
                .HasKey(x => x.Id);

            builder.Entity<Product>()
                .HasKey(x => x.Id);

            builder.Entity<BookAuthor>()
                .HasKey(x => new { x.AuthorId, x.ProductId });
            builder.Entity<BookAuthor>()
                .HasOne<Author>(x => x.Author)
                .WithMany(m => m.BookAuthors)
                .HasForeignKey(x => x.AuthorId);
            builder.Entity<BookAuthor>()
                .HasOne<Product>(x => x.Product)
                .WithMany(e => e.BookAuthors)
                .HasForeignKey(x => x.ProductId);
        }
        public DbSet<Product> Products { get; set; }
        public DbSet<Author> Authors { get; set; }
        public DbSet<Token> Tokens { get; set; }
        public DbSet<BookAuthor> BookAuthors { get; set; }        
    }
}

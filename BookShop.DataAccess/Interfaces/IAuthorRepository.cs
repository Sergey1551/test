﻿using BookShop.DataAccess.Entities;
using System.Threading.Tasks;

namespace BookShop.DataAccess.Interfaces
{
    public interface IAuthorRepository : IGenericRepository<Author>
    {
        Task<Author> FindByName(string name);
    }
}

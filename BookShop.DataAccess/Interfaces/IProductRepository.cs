﻿using BookShop.DataAccess.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BookShop.DataAccess.Interfaces
{
    public interface IProductRepository : IGenericRepository<Product>
    {
        Task<Product> FindByName(string name);
        Task<Product> Find(int id);
        Task<IEnumerable<Product>> GetProducts();    
    }
}

﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace BookShop.DataAccess.Interfaces
{
    public interface IGenericRepository<TEntity> where TEntity : class
    {
        Task Create(TEntity item);
        Task<IEnumerable<TEntity>> Get();
        Task Remove(TEntity item);
        Task Remove(IEnumerable<TEntity> items);
        Task Update(TEntity item);
    }
}
 
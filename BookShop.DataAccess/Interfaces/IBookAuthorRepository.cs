﻿using BookShop.DataAccess.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BookShop.DataAccess.Interfaces
{
    public interface IBookAuthorRepository : IGenericRepository<BookAuthor>
    {
        Task Add (IEnumerable<BookAuthor> authorProducts);
        Task<IEnumerable<BookAuthor>> GetById(int id);
    }
}

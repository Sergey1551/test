﻿using BookShop.DataAccess.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookShop.DataAccess.Interfaces
{
    public interface ITokenRepository : IGenericRepository<Token>
    {
        Task<IEnumerable<Token>> GetTokens(string id);
        Task<Token> Find(string ClientId, string RefreshToken);
    }
}

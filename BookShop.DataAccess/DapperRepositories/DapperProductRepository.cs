﻿using BookShop.DataAccess.Entities;
using BookShop.DataAccess.Interfaces;
using Dapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace BookShop.DataAccess.DapperRepositories
{
    public class DapperProductRepository : DapperGenericRepository<Product>, IProductRepository
    {
        private readonly IConfiguration _configuration;
        private readonly ConfigSettings _configSettings;
        private readonly DapperBookAuthorRepository _bookAuthorRepository;
        public DapperProductRepository(IConfiguration configuration, IOptions<ConfigSettings> configSettings, DapperBookAuthorRepository bookAuthorRepository) : base(configuration, "Products", configSettings.Value)
        {
            _configuration = configuration;
            _configSettings = configSettings.Value;
            _bookAuthorRepository = bookAuthorRepository;
        }

        public async Task<Product> FindByName(string name)
        {
            using (IDbConnection db = new SqlConnection(_configuration.GetConnectionString(_configSettings.Connection)))
            {
                var product = await db.QueryFirstOrDefaultAsync<Product>($"SELECT * FROM  Products  WHERE Name = @name", new { name });

                return product;
            }
        }

        public async Task<Product> Find(int id)
        {
            using (IDbConnection db = new SqlConnection(_configuration.GetConnectionString(_configSettings.Connection)))
            {
                var product = await db.QueryFirstOrDefaultAsync<Product>($"SELECT * FROM  Products  WHERE Id = @id", new { id });

                return product;
            }
        }

        public async Task<IEnumerable<Product>> GetProducts()
        {
            using (IDbConnection db = new SqlConnection(_configuration.GetConnectionString(_configSettings.Connection)))
            {
                var bookAuthors = await _bookAuthorRepository.GetAll();

                var products = db.Query<Product>("SELECT * FROM Products").Select(x => new Product()
                {
                    Id = x.Id,
                    BookAuthors = bookAuthors.Where(e => e.ProductId == x.Id).ToList(),
                    Description = x.Description,
                    OutOfStock = x.OutOfStock,
                    Price = x.Price,
                    Name = x.Name,
                    Type = x.Type
                }).ToList();

                return products;                
            }
        }
    }
}
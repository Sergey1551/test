﻿using BookShop.DataAccess.Entities;
using BookShop.DataAccess.Interfaces;
using Dapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace BookShop.DataAccess.DapperRepositories
{
    public class DapperAuthorRepository : DapperGenericRepository<Author>, IAuthorRepository
    {
        private readonly IConfiguration _configuration;
        private readonly ConfigSettings _configSettings;
        public DapperAuthorRepository(IConfiguration configuration, IOptions<ConfigSettings> configSettings) : base(configuration, "Authors", configSettings.Value)
        {
            _configSettings = configSettings.Value;
            _configuration = configuration;
        }

        public async Task<Author> FindByName(string name)
        {
            using (IDbConnection db = new SqlConnection(_configuration.GetConnectionString(_configSettings.Connection)))
            {
                var author = await db.QueryFirstOrDefaultAsync<Author>($"SELECT * FROM  Authors  WHERE Name = @name", new { name });

                return author;
            }
        }
    }
}

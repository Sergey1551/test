﻿using BookShop.DataAccess.Interfaces;
using Dapper;
using Dapper.Contrib.Extensions;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace BookShop.DataAccess.DapperRepositories
{
    public class DapperGenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class
    {
        private readonly IConfiguration _configuration;
        protected readonly string _tableName;
        private readonly string _connection;

        public DapperGenericRepository(IConfiguration configuration, string tableName, ConfigSettings connection)
        {
            _configuration = configuration;
            _tableName = tableName;
            _connection = connection.Connection;
        }

        public async Task Create(TEntity item)
        {
            using (IDbConnection db = new SqlConnection(_configuration.GetConnectionString(_connection)))
            {
                await db.InsertAsync(item);
            }
        }

        public async Task<IEnumerable<TEntity>> Get()
        {
            using (IDbConnection db = new SqlConnection(_configuration.GetConnectionString(_connection)))
            {
                var results = await db.QueryAsync<TEntity>($"SELECT * FROM {_tableName}");

                return results;
                
            }
        }
        
        public async Task Remove(TEntity item)
        {
            using (IDbConnection db = new SqlConnection(_configuration.GetConnectionString(_connection)))
            {
                await db.DeleteAsync(item);
            }
        }

        public async Task Remove(IEnumerable<TEntity> items)
        {
            using (IDbConnection db = new SqlConnection(_configuration.GetConnectionString(_connection)))
            {
                await db.DeleteAsync(items);
            }
        }

        public async Task Update(TEntity item)
        {
            using (IDbConnection db = new SqlConnection(_configuration.GetConnectionString(_connection)))
            {
                await db.UpdateAsync(item);
            }
        }
    }
}

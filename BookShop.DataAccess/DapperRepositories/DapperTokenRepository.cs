﻿using BookShop.DataAccess.Entities;
using BookShop.DataAccess.Interfaces;
using Dapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace BookShop.DataAccess.DapperRepositories
{
    public class DapperTokenRepository : DapperGenericRepository<Token>, ITokenRepository
    {
        private readonly IConfiguration _configuration;
        private readonly ConfigSettings _configSettings;
        public DapperTokenRepository(IConfiguration configuration, IOptions<ConfigSettings> configSettings) : base(configuration, "Tokens", configSettings.Value)
        {
            _configuration = configuration;
            _configSettings = configSettings.Value;
        }

        public async Task<Token> Find(string clientId, string refreshToken)
        {
            using (IDbConnection db = new SqlConnection(_configuration.GetConnectionString(_configSettings.Connection)))
            {
                var token = await db.QueryFirstOrDefaultAsync<Token>($"SELECT 1 FROM  Tokens  WHERE ClientId = @clientId AND Value = @refreshToken", new { clientId, refreshToken });

                return token;
            }
        }

        public async Task<IEnumerable<Token>> GetTokens(string id)
        {
            using (IDbConnection db = new SqlConnection(_configuration.GetConnectionString(_configSettings.Connection)))
            {
                var tokens = await db.QueryAsync<Token>($"SELECT * FROM Tokens WHERE UserId = @id", new { id});

                return tokens;
            }
        }
    }
}

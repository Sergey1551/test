﻿using BookShop.DataAccess.Entities;
using BookShop.DataAccess.Interfaces;
using Dapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace BookShop.DataAccess.DapperRepositories
{
    public class DapperUserRepository : DapperGenericRepository<ApplicationUser>, IUserRepository
    {
        private readonly IConfiguration _configuration;
        private readonly ConfigSettings _configSettings;
        public DapperUserRepository(IConfiguration configuration, IOptions<ConfigSettings> configSettings) : base(configuration, "AspNetUsers", configSettings.Value)
        {
            _configuration = configuration;
            _configSettings = configSettings.Value;
        }

        public async Task<ApplicationUser> Find(string userName)
        {
            using (IDbConnection db = new SqlConnection(_configuration.GetConnectionString(_configSettings.Connection)))
            {
                var user = await db.QueryFirstOrDefaultAsync<ApplicationUser>($"SELECT * FROM AspNetUsers WHERE UserName = @userName", new { userName });

                return user;
            }
        }
    }
}

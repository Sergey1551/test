﻿using BookShop.DataAccess.Entities;
using BookShop.DataAccess.Interfaces;
using Dapper;
using Dapper.Contrib.Extensions;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace BookShop.DataAccess.DapperRepositories
{
    public class DapperBookAuthorRepository : DapperGenericRepository<BookAuthor>, IBookAuthorRepository
    {
        private readonly IConfiguration _configuration;
        private readonly ConfigSettings _configSettings;
        public DapperBookAuthorRepository(IConfiguration configuration, IOptions<ConfigSettings> configSettings) : base(configuration, "BookAuthors", configSettings.Value)
        {
            _configuration = configuration;
            _configSettings = configSettings.Value;
        }

        public async Task Add(IEnumerable<BookAuthor> authorProducts)
        {
            using (IDbConnection db = new SqlConnection(_configuration.GetConnectionString(_configSettings.Connection)))
            {
                await db.InsertAsync<IEnumerable<BookAuthor>>(authorProducts);
            }
        }
        public async Task<IEnumerable<BookAuthor>> GetAll()
        {
            using (IDbConnection db = new SqlConnection(_configuration.GetConnectionString(_configSettings.Connection)))
            {
                var bookAuthors = await db.QueryAsync<BookAuthor, Author, BookAuthor>($@"SELECT * FROM  BookAuthors as BA INNER JOIN Authors as A ON A.Id = BA.AuthorId", (BA, A) =>
                {
                    BA.Author = A;

                    return BA;
                });

                return bookAuthors;
            }
        }

        public async Task<IEnumerable<BookAuthor>> GetById(int id)
        {
            using (IDbConnection db = new SqlConnection(_configuration.GetConnectionString(_configSettings.Connection)))
            {
                var bookAuthors = await db.QueryAsync<BookAuthor>($"SELECT * FROM BookAuthors WHERE ProductId = @id", new { id });

                return bookAuthors;
            }
        }
    }
}

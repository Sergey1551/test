﻿using Dapper.Contrib.Extensions;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;

namespace BookShop.DataAccess.Entities
{
    [Table("AspNetUsers")]
    public class ApplicationUser : IdentityUser
    {
        public string Notes { get; set; }

        public int Type { get; set; }

        public string DisplayName { get; set; }

        public virtual List<Token> Tokens { get; set; } = new List<Token> { };
    }
}

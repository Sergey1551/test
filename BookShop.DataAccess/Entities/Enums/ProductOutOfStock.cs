﻿namespace BookShop.DataAccess.Entities.Enums
{
    public class ProductOutOfStock
    {
        public enum Out
        {
            no = 0,
            yes = 1
        }
    }
}

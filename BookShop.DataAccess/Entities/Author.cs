﻿using Dapper.Contrib.Extensions;
using System.Collections.Generic;

namespace BookShop.DataAccess.Entities
{
    public class Author : BaseEntity
    {
        public string Name { get; set; }

        [Write(false)]
        public ICollection<BookAuthor> BookAuthors { get; set; } = new List<BookAuthor> { };
    }
}
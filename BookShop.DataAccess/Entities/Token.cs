﻿using Dapper.Contrib.Extensions;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace BookShop.DataAccess.Entities
{
    public class Token
    {
        [System.ComponentModel.DataAnnotations.Key]
        public int Id { get; set; }

        public string ClientId { get; set; }

        public string Value { get; set; }

        public DateTime CreationDate { get; set; }

        public string UserId { get; set; }

        public DateTime LastModifiedTime { get; set; }

        public DateTime ExpireTime { get; set; }

        [ForeignKey("UserId")]
        [Write(false)]
        public ApplicationUser User { get; set; }
    }
}

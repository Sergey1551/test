﻿using Dapper.Contrib.Extensions;
using System.Collections.Generic;

namespace BookShop.DataAccess.Entities
{
    public class Product : BaseEntity
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public bool OutOfStock { get; set; }

        [Write(false)]
        public ICollection<BookAuthor> BookAuthors { get; set; } = new List<BookAuthor> { };
       
        public decimal Price { get; set; }

        public string Type { get; set; }
    }
}
﻿using Dapper.Contrib.Extensions;

namespace BookShop.DataAccess.Entities
{
    public class BookAuthor : BaseEntity
    {
        public long AuthorId { get; set; }

        [Write(false)]
        public Author Author { get; set; }

        public long ProductId { get; set; }

        [Write(false)]
        public Product Product { get; set; }
    }
}
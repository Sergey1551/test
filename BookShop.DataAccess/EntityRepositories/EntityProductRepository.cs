﻿using BookShop.DataAccess.Entities;
using BookShop.DataAccess.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookShop.DataAccess.EntityRepositories
{
    public class EntityProductRepository : EntityGenericRepository<Product>, IProductRepository
    {
        private readonly ApplicationContext _db;

        public EntityProductRepository(ApplicationContext db, ApplicationContext context) : base(context)
        {
            _db = db;
        }

        public async Task<Product> Find(int id)
        {
            var product = await _db.Products.FirstOrDefaultAsync(p => p.Id == id);

            return product;
        }

        public async Task<Product> FindByName(string name)
        {
            var product = await _db.Products.FirstOrDefaultAsync(p => p.Name == name);

            return product;
        }

        public async Task<IEnumerable<Product>> GetProducts()
        {
            var products = await _db.Products
                .Include(x => x.BookAuthors)
                .ThenInclude(x => x.Author)
                .ToListAsync();

            return products;
        }
    }
}

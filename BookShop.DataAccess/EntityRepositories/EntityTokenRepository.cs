﻿using BookShop.DataAccess.Entities;
using BookShop.DataAccess.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookShop.DataAccess.EntityRepositories
{
    public class EntityTokenRepository : EntityGenericRepository<Token>, ITokenRepository
    {
        private readonly ApplicationContext _db;

        public EntityTokenRepository(ApplicationContext db, ApplicationContext context) : base(context)
        {
            _db = db;
        }

        public async Task<IEnumerable<Token>> GetTokens(string id)
        {
            var tokens = await _db.Tokens.Where(rt => rt.UserId == id).ToListAsync();

            return tokens;
        }

        public async Task<Token> Find(string ClientId, string RefreshToken)
        {
            var token = await _db.Tokens.Where(t => t.ClientId == ClientId && t.Value == RefreshToken).FirstOrDefaultAsync();

            return token;
        }
    }
}

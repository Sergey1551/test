﻿using BookShop.DataAccess.Entities;
using BookShop.DataAccess.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace BookShop.DataAccess.EntityRepositories
{
    public class EntityUserRepository : EntityGenericRepository<ApplicationUser>, IUserRepository
    {
        private readonly ApplicationContext _db;

        public EntityUserRepository(ApplicationContext db, ApplicationContext context) : base(context)
        {
            _db = db;
        }

        public async Task<ApplicationUser> Find(string userName)
        {
            var user = await _db.Users.Where(p => p.UserName == userName).FirstOrDefaultAsync();

            return user;
        }
    }
}

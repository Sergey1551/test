﻿using BookShop.DataAccess.Entities;
using BookShop.DataAccess.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookShop.DataAccess.EntityRepositories
{
    public class EntityBookAuthorRepository : EntityGenericRepository<BookAuthor>, IBookAuthorRepository
    {
        private readonly ApplicationContext _db;

        public EntityBookAuthorRepository(ApplicationContext db, ApplicationContext context) : base (context)
        {
            _db = db;
        }
        public async Task Add(IEnumerable<BookAuthor> authorProducts)
        {
            foreach(var a in authorProducts)
            {
                await _db.BookAuthors.AddAsync(a);
            }

            await _db.SaveChangesAsync();
        }

        public async Task<IEnumerable<BookAuthor>> GetById(int id)
        {
            var bookAuthors = await _db.BookAuthors.Where(x => x.ProductId == id).ToListAsync();

            return bookAuthors;
        }
    }
}

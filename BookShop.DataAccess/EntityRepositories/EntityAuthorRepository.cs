﻿using BookShop.DataAccess.Entities;
using BookShop.DataAccess.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace BookShop.DataAccess.EntityRepositories
{
    public class EntityAuthorRepository : EntityGenericRepository<Author>, IAuthorRepository
    {
        private readonly ApplicationContext _db;

        public EntityAuthorRepository(ApplicationContext db, ApplicationContext context) : base(context) 
        {
            _db = db;
        }

        public async Task<Author> FindByName(string name)
        {
            var author = await _db.Authors.FirstOrDefaultAsync(x => x.Name == name);

            return author;
        }
    }
}

﻿using BookShop.BusinessLogic.Services.Interfaces;
using BookShop.DataAccess.Entities;
using BookShop.DataAccess.Interfaces;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BookShop.BusinessLogic.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;

        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task<IEnumerable<ApplicationUser>> GetListOfUsers()
        {
            var users = await _userRepository.Get();

            return users;
        }

        public async Task<ApplicationUser> FindUser(string userName)
        {
            var user = await _userRepository.Find(userName);

            return user;
        }

        public async Task DeleteUserAsync(ApplicationUser foundUser)
        {
            await _userRepository.Remove(foundUser);
        }
    }
}

﻿using BookShop.BusinessLogic.Services.Interfaces;
using BookShop.DataAccess.Entities;
using BookShop.DataAccess.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookShop.BusinessLogic.Services
{
    public class TokenService : ITokenService
    {     
        private readonly ITokenRepository _tokenRepository;

        public TokenService(ITokenRepository tokenRepository)
        {
            _tokenRepository = tokenRepository;
        }

        public async Task<IEnumerable<Token>> OldRefreshTokens(string id)
        {
            return await _tokenRepository.GetTokens(id);
        }

        public async Task RemoveOldToken(Token oldToken)
        {
           await _tokenRepository.Remove(oldToken);
        }

        public async Task AddNewTokenAsync(Token model)
        {
            await _tokenRepository.Create(model);
        }

        public async Task<Token> FindToken(string ClientId, string RefreshToken)
        {
            return await _tokenRepository.Find(ClientId, RefreshToken);
        }
    }
}

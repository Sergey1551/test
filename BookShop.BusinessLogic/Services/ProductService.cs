﻿using BookShop.DataAccess.Entities;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using BookShop.BusinessLogic.ViewModels;
using BookShop.DataAccess.Interfaces;
using BookShop.BusinessLogic.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace BookShop.BusinessLogic.Services
{
    public class ProductService : IProductService
    {
        private readonly IProductRepository _productRepository;
        private readonly IBookAuthorRepository _bookAuthorRepository;
        private readonly IAuthorRepository _authorRepository;

        public ProductService(IProductRepository productRepository, IBookAuthorRepository bookAuthorRepository, IAuthorRepository authorRepository)
        {
            _productRepository = productRepository;
            _bookAuthorRepository = bookAuthorRepository;
            _authorRepository = authorRepository;
        }

        public async Task<IActionResult> AddProductAsync(AddProductView model)
        {
            var isProductExist = await _productRepository.FindByName(model.Name);

            if (isProductExist != null)
            {
                return (new JsonResult("Product is already exist"));
            }

            
            var newProduct = new Product
            {
                Name = model.Name,
                Description = model.Description,
                OutOfStock = model.OutOfStock,
                Price = model.Price,
                Type = model.Type,
            };

            var authorProducts = model.AuthorIds.Select(x => new BookAuthor()
            {
                AuthorId = x,
                ProductId = newProduct.Id
            });

            await _productRepository.Create(newProduct);
            await _bookAuthorRepository.Add(authorProducts);

            return new JsonResult("The Product has beed added successfully");
            

        }
     
        public async Task<IActionResult> AddAuthorAsync(AddAuthorProductView model)
        {
            var isAuthorExist = await _authorRepository.FindByName(model.Name);
            
            if(isAuthorExist == null)
            {
                var newAuthor = new Author
                {
                    Name = model.Name
                };

                await _authorRepository.Create(newAuthor);
                return new JsonResult("Author has been added successully");
            }

            return new JsonResult("This author is already exist");
        }

        public async Task<Product> FindProduct(int id)
        {
            return await _productRepository.Find(id);
        }

        public async Task UpdateProductAsync(int id, UpdateProductView model)
        {
            var foundProduct = await FindProduct(id);
            
            foundProduct.Name = model.Name;
            foundProduct.Description = model.Description;
            foundProduct.OutOfStock = model.OutOfStock;
            foundProduct.Price = model.Price;
            foundProduct.Type = model.Type;

            var oldAutorProducts = await _bookAuthorRepository.GetById(id);
            var authorProducts = model.AuthorIds.Select(x => new BookAuthor()
            {
                AuthorId = x,
                ProductId = foundProduct.Id
            });

            var authorsToRemove = oldAutorProducts.Where(x => x.ProductId == id);

            await _bookAuthorRepository.Remove(authorsToRemove);
            await _bookAuthorRepository.Add(authorProducts);
            await _productRepository.Update(foundProduct);
        }

        public async Task DeleteProductAsync(Product foundProduct)
        {
            await _productRepository.Remove(foundProduct);
        }

        public async Task<IEnumerable<GetProductsView>> GetListOfProducts()
        {
            var products = await _productRepository.GetProducts();

            var listOfProducts = products.Select(x => new GetProductsView()
            {
                ProductId = x.Id,
                Description = x.Description,
                OutOfStock = x.OutOfStock,
                Price = x.Price,
                Type = x.Type,
                Name = x.Name,
                Authors = x.BookAuthors?.Where(e => e.ProductId == x.Id).Select(e => new Author()
                {
                    Id = e.AuthorId,
                    Name = e.Author.Name,
                }).ToList()
            }).ToList();

            return listOfProducts;
        }

        public async Task<IEnumerable<GetAuthorsProductView>> GetListOfAuthors()
        {
            var authors = await _authorRepository.Get();

            var authorsList = authors.Select(x => new GetAuthorsProductView()
            {
                AuthorId = x.Id,
                Name = x.Name
            }).ToList();

            return authorsList;
        }
    }
}
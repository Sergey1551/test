﻿using BookShop.BusinessLogic.ViewModels;
using BookShop.DataAccess.Entities;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BookShop.BusinessLogic.Services.Interfaces
{
    public interface IProductService
    {
        Task<IActionResult> AddProductAsync(AddProductView model);
        Task<IActionResult> AddAuthorAsync(AddAuthorProductView model);
        Task<Product> FindProduct(int id);
        Task UpdateProductAsync(int id,UpdateProductView model);
        Task DeleteProductAsync(Product findProduct);
        Task<IEnumerable<GetProductsView>> GetListOfProducts();
        Task<IEnumerable<GetAuthorsProductView>> GetListOfAuthors();
    }
}

﻿using BookShop.DataAccess.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BookShop.BusinessLogic.Services.Interfaces
{
    public interface ITokenService
    {
        Task<IEnumerable<Token>> OldRefreshTokens(string id);
        Task RemoveOldToken(Token oldToken);
        Task AddNewTokenAsync(Token model);
        Task<Token> FindToken(string ClientId, string RefreshToken);
    }
}

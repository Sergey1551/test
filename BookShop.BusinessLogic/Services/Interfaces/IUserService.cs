﻿using BookShop.DataAccess.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BookShop.BusinessLogic.Services.Interfaces
{
    public interface IUserService
    {
        Task<IEnumerable<ApplicationUser>> GetListOfUsers();
        Task<ApplicationUser> FindUser(string userName);
        Task DeleteUserAsync(ApplicationUser foundUser);
    }
}

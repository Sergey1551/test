﻿using System.ComponentModel.DataAnnotations;

namespace BookShop.BusinessLogic.ViewModels
{
    public class RegisterIdentityView
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Name")]
        public string UserName { get; set; }
      
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}

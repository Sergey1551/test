﻿using System.ComponentModel.DataAnnotations;

namespace BookShop.BusinessLogic.ViewModels
{
    public class LoginIdentityView
    {
        [Required]
        [Display(Name = "Email")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}

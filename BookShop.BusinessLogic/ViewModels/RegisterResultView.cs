﻿namespace BookShop.BusinessLogic.ViewModels
{
    public class RegisterResultView
    {
        public string UserName { get; set; }

        public string Email { get; set; }

        public int Status { get; set; }

        public string Message { get; set; }
    }
}
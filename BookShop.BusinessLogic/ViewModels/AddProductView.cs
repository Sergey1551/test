﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BookShop.BusinessLogic.ViewModels
{
    public class AddProductView
    {

        [Required]
        [MaxLength(50)]
        public string Name { get; set; }

        [Required]
        [MaxLength(150)]
        public string Description { get; set; }

        [Required]
        public bool OutOfStock { get; set; }

        [Required]
        public ICollection<long> AuthorIds { get; set; }

        [Required]
        public decimal Price { get; set; }
        [Required]
        public string Type { get; set; }
    }
}

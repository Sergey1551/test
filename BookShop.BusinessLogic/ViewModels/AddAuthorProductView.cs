﻿using BookShop.DataAccess.Entities;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BookShop.BusinessLogic.ViewModels
{
    public class AddAuthorProductView
    {

        [Required]
        public string Name { get; set; }
    }
}
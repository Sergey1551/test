﻿using BookShop.DataAccess.Entities;
using System.Collections.Generic;

namespace BookShop.BusinessLogic.ViewModels
{
    public class GetProductsView
    {
        public long ProductId { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public bool OutOfStock { get; set; }

        public ICollection<Author> Authors { get; set; }

        public decimal Price { get; set; }

        public string Type { get; set; }

    }
}

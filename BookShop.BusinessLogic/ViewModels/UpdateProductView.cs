﻿using BookShop.DataAccess.Entities;
using System.Collections.Generic;

namespace BookShop.BusinessLogic.ViewModels
{
    public class UpdateProductView
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public bool OutOfStock { get; set; }

        public ICollection<long> AuthorIds { get; set; }

        public decimal Price { get; set; }

        public string Type { get; set; }

        public virtual ApplicationUser User { get; set; }
    }
}
